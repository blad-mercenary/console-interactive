# Console (Interactive)

[![pipeline status](https://gitlab.com/blad-mercenary/console/badges/dev/pipeline.svg)](https://gitlab.com/blad-mercenary/console/commits/dev) [![coverage report](https://gitlab.com/blad-mercenary/console/badges/dev/coverage.svg)](https://gitlab.com/blad-mercenary/console/commits/dev)

Console (terminal) intercative implentation of the game.

To run, simply use `npm start`.

You can also use `npm link` to install locally and run `npx @blad-mercenary/console-interactive`

The default savefile is `savefile.json` in the project folder.

`--help`: Show help
`--new` / `-n`: Create a new fight, deleting the current savefile.
`--preset [index]` / `-p [index]`: Use fight preset (from 0 to 0). Implies --new.
`--file [path]` / `-f [path]`: Path of the savefile to use.
`--logs`: Pretty print the fight's logs.

Presets:

See [the `presets.ts` file](./lib/presets.ts) for the current list.

| Index | Ally | Enemies |
|---|---|---|
| 0 | 2 Warriors | 2 Mages |
