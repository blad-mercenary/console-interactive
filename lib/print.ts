import chalk from 'chalk';
import { Character, Team, Selection, Target, Event, Fight } from '@blad-mercenary/core';
import * as EventLogger from './event-logger';

const length = 20;

function teamColor(team: Team) {
	if (team === Team.player) {
		return chalk.green;
	} else {
		return chalk.red;
	}
}

export function name(character: Character): string {
	return teamColor(character.team)(character.name);
}

export function selection(selection: Selection): string {
	if (selection.action.possibleTarget === Target.self) {
		return selection.action.name;
	}
	return `${selection.action.name} ${selection.targets.map((character) => character.name).join(',')}`;
}

export function event(event: Event, fighters: Character[]): void {
	if(event.type === 'actionUsed') {
		EventLogger.actionUsed(event, fighters);
	}

	if(event.type === 'characterDied') {
		EventLogger.characterDied(event);
	}

	if(event.type === 'victory') {
		EventLogger.victory(event);
	}

	if(event.type === 'playerSave') {
		EventLogger.playerSave();
	}

	if(event.type === 'playerLoad') {
		EventLogger.playerLoad();
	}

	if(event.type === 'damage') {
		EventLogger.damage(event, fighters);
	}

	if(event.type === 'heal') {
		EventLogger.heal(event, fighters);
	}

	if(event.type === 'resurrect') {
		EventLogger.resurrect(event, fighters);
	}

	if(event.type === 'noAction') {
		EventLogger.noAction(event, fighters);
	}
}

interface FormattedCharacter {
	index: string,
	id: string,
	team: Team,
	type: string,
	name: string,
	health: string,
	healthBar: string,
	mana: string,
	manaBar: string,
	init: string
}

function formatCharacter(fighter: Character, index: number): FormattedCharacter {
	function getBar(cur: number, max: number): string {
		const percent = cur / max * (length - 2);
		return `[${'='.repeat(percent)}${' '.repeat(length - 2 - percent)}]`;
	}

	// console.debug(`${fighter.name}: ${fighter.lastTurn}`);
	return {
		index: `[${index}]`,
		id: fighter.id,
		team: fighter.team,
		name: fighter.name,
		type: fighter.type,
		// status: fighter.status.map(),
		health: `${fighter.health} / ${fighter.maxHealth}`,
		healthBar: getBar(fighter.health, fighter.maxHealth),
		mana: `${fighter.mana} / ${fighter.maxMana}`,
		manaBar: getBar(fighter.mana, fighter.maxMana),
		init: `${fighter.init} / ${fighter.lastTurn}`
	};
}

function printProperty(
	characters: FormattedCharacter[],
	property: keyof FormattedCharacter,
	color: chalk.Chalk = chalk.reset
): void {
	console.log(
		color(
			characters.map((character) => {
				const propertyString = `${character[property]}`;
				const data = propertyString.length > length
					? propertyString.slice(0, length)
					: propertyString.padEnd(length);
				return data;
			}).join('')
		)
	);
}

export function characterBar(characters: FormattedCharacter[], team: Team): void {
	printProperty(characters, 'index', teamColor(team));
	printProperty(characters, 'name', teamColor(team));
	printProperty(characters, 'type');
	printProperty(characters, 'health', chalk.green);
	printProperty(characters, 'healthBar', chalk.green);
	printProperty(characters, 'mana', chalk.blueBright);
	printProperty(characters, 'manaBar', chalk.blueBright);
	printProperty(characters, 'init', chalk.yellow);
}

export function status(fight: Fight): void {
	const fighters = fight.fighters.map(formatCharacter);
	const allies = fighters.filter((fighter) => fighter.team === Team.player);
	const enemies = fighters.filter((fighter) => fighter.team === Team.enemy);
	const activeFighters = fight
		.getNextActiveFighters()
		.map(((aF) => fighters.find((f) => f.id === aF.id)))
		.map((fighter) => teamColor((fighter as FormattedCharacter).team)((fighter as FormattedCharacter).index))
		.join('');

	// Print full character informations
	// Name, team, type, health, mana, init
	console.log('Allies:');
	characterBar(allies, Team.player);
	console.log('Enemies:');
	characterBar(enemies, Team.enemy);

	// Print next characters bar
	// (Only character indexes with team color)
	console.log(activeFighters);
}