import { Character, Characters, Team } from '@blad-mercenary/core';

export interface Preset {
	allies: Character[];
	enemies: Character[];
}

export const preset = [
	{
		allies: [Characters.warrior(Team.player), Characters.warrior(Team.player)],
		enemies: [Characters.mage(Team.enemy), Characters.mage(Team.enemy)]
	}
];
