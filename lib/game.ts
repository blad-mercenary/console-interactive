import inquirer from 'inquirer';
import {
	Character, Characters, CharacterType, Fight, Selection, Team
} from '@blad-mercenary/core';
import { load, save } from './savefile';
import * as print from './print';
import { preset } from './presets';

export async function getTeam(team: Team): Promise<Character[]> {
	const characterTypes = Object.keys(Characters);
	const characters = [];
	let stop = false;

	while (!stop) {
		const { characterType } = await inquirer.prompt({
			type: 'list',
			name: 'characterType',
			message: `Add to ${team}'s team`,
			choices: [...characterTypes, { name: '-Done-', value: null }]
		});

		if (characterType) {
			characters.push(Characters[characterType as CharacterType](team));
		} else {
			stop = true;
		}
	}

	return characters;
}

export async function start(savefile?: string, forceNew?: boolean, presetIndex?: number): Promise<void> {
	// If asked to create a new fight, initialize it
	if (forceNew || !savefile) {
		let allies;
		let enemies;

		if (presetIndex !== undefined) {
			allies = preset[presetIndex].allies;
			enemies = preset[presetIndex].enemies;
		} else {
			allies = await getTeam(Team.player);
			enemies = await getTeam(Team.enemy);
		}

		const fight = new Fight([...allies, ...enemies]);
		await save(fight, savefile);
	}

	// On graphical client, load the fight. No need on this client.
}

export async function getPlayerAction(fight: Fight, activeFighter: Character): Promise<Selection> {
	const possibleSelections = Selection.getPossibleSelections(activeFighter, fight);

	const { action } = await inquirer.prompt({
		type: 'list',
		name: 'action',
		message: `${activeFighter.name}:`,
		choices: possibleSelections.map((selection) => {
			return {
				name: print.selection(selection),
				value: selection
			};
		})
	});

	return action;
}

export async function loop(savefile?: string): Promise<boolean> {
	const fight = await load(savefile);

	// Print fight
	// On graphical client, refresh screen state
	print.status(fight);

	const beforeTurn = new Date();
	const activeFighter = fight.getActiveFighter();

	if (activeFighter.team === Team.enemy) {
		// On enemy, auto-step
		fight.step();
	} else {
		// On player, ask for their action
		const action = await getPlayerAction(fight, activeFighter);
		// Use it for step
		fight.step(action);
	}

	// Fetch turn's events
	const turnEvents = fight.eventLogger.getEvents(beforeTurn);
	// Log them
	// On graphical event, this would be where animations comes up
	turnEvents.forEach((event) => {
		print.event(event, fight.fighters);
	});

	console.log('-'.repeat(process.stdout.columns));
	await save(fight, savefile);
	const { next } = await inquirer.prompt({ type: 'confirm', name: 'next', message: 'Go to next turn ?' });

	// Break the loop if the fight ended
	return fight.finished || !next;
}

export async function log(savefile?: string): Promise<void> {
	const fight = await load(savefile);

	fight.eventLogger.getEvents().forEach((event) => {
		print.event(event, fight.fighters);
	});
}
