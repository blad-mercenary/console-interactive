import {
	Event,
	Character,
	ActionUsedEventDetails,
	CharacterDiedEventDetails,
	VictoryEventDetails,
	DamageEventDetails,
	HealEventDetails,
	ResurrectEventDetails,
	NoActionEventDetails,
	Action,
	Team,
} from '@blad-mercenary/core';
import * as print from './print';

function name(id: string, fighters: Character[]): string {
	const fighter = fighters.find((fighter) => fighter.id === id);
	if (!fighter) {
		throw new Error(`Cannot find id ${id}`);
	}
	return print.name(fighter);
}

export function actionUsed(event: Event, fighters: Character[]): void {
	const details = event.details as ActionUsedEventDetails;
	const from = name(details.from, fighters);
	const action = Action.load(details.action);
	const targets = details.targetsIds.map((target) => name(target, fighters));
	console.log(`${from} used ${action.name} on ${targets.join(', ')}`);
}

export function characterDied(event: Event): void {
	const details = event.details as CharacterDiedEventDetails;
	const character = details.characterId;
	console.log(`${character} died`);
}

export function victory(event: Event): void {
	const details = event.details as VictoryEventDetails;
	if (details.winner === Team.player) {
		console.log('You won');
	} else {
		console.log('You lose');
	}
}

export function playerSave(): void {
	console.log('Player save');
}

export function playerLoad(): void {
	console.log('Player load');
}

export function damage(event: Event, fighters: Character[]): void {
	const details = event.details as DamageEventDetails;
	const from = name(details.from, fighters);
	const target = name(details.target, fighters);
	console.log(`${from} inflicted ${details.damage} damage to ${target}`);
}

export function heal(event: Event, fighters: Character[]): void {
	const details = event.details as HealEventDetails;
	const from = name(details.from, fighters);
	const target = name(details.target, fighters);
	console.log(`${from} healed ${target} of ${details.heal} damage`);
}

export function resurrect(event: Event, fighters: Character[]): void {
	const details = event.details as ResurrectEventDetails;
	const from = name(details.from, fighters);
	const target = name(details.target, fighters);
	console.log(`${from} resurected ${target}`);
}

export function noAction(event: Event, fighters: Character[]): void {
	const details = event.details as NoActionEventDetails;
	const from = name(details.from, fighters);
	console.log(`${from} did nothing`);
}
