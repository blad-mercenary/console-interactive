import { describe, it, afterEach } from 'mocha';
import * as sinon from 'sinon';
import sinonChai from 'sinon-chai';
import fs from 'fs/promises';
import * as chai from 'chai';
import * as game from '../../lib/game';

chai.use(sinonChai);
const { expect } = chai;

describe('game', () => {
	describe('getTeam', () => {
		it('loops until the stop option is selected', () => {});
		it('returns the character list', () => {});
	});

	describe('start', () => {
		it('does nothing if loading a fight', () => {});
		it('creates a fight if there is no savefile', () => {});
		it('creates a fight if "new" flag is set', () => {});
		it('loads the preset if given one', () => {});
		it('lets user create the teams if no preset', () => {});
	});

	describe('getPlayerAction', () => {
		it('prompt all possible selections', () => {});
	});

	describe('loop', () => {
		it('loads before and save after the turn', () => {});
		it('returns a boolean indicating if the fight ended', () => {});
		it('auto step on enemy turn', () => {});
		it('asks for player input and step on player turn', () => {});
		it('prints status and current turn\'s event', () => {});
	});

	describe('log', () => {
		it('prints all event from a file', () => {});
	});
});