"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.preset = void 0;
var core_1 = require("@blad-mercenary/core");
exports.preset = [
    {
        allies: [core_1.Characters.warrior(core_1.Team.player), core_1.Characters.warrior(core_1.Team.player)],
        enemies: [core_1.Characters.mage(core_1.Team.enemy), core_1.Characters.mage(core_1.Team.enemy)]
    }
];
