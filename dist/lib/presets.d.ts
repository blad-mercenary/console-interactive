import { Character } from '@blad-mercenary/core';
export interface Preset {
    allies: Character[];
    enemies: Character[];
}
export declare const preset: {
    allies: Character[];
    enemies: Character[];
}[];
