import { Character, Team, Selection, Event, Fight } from '@blad-mercenary/core';
export declare function name(character: Character): string;
export declare function selection(selection: Selection): string;
export declare function event(event: Event, fighters: Character[]): void;
interface FormattedCharacter {
    index: string;
    id: string;
    team: Team;
    type: string;
    name: string;
    health: string;
    healthBar: string;
    mana: string;
    manaBar: string;
    init: string;
}
export declare function characterBar(characters: FormattedCharacter[], team: Team): void;
export declare function status(fight: Fight): void;
export {};
