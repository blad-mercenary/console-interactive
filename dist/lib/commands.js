"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.showStatus = void 0;
var core_1 = require("@blad-mercenary/core");
var chalk_1 = __importDefault(require("chalk"));
function format(character) {
    return [
        character.team === core_1.Team.player
            ? chalk_1.default.green(character.name + " (" + character.id + ")")
            : chalk_1.default.red(character.name + " (" + character.id + ")"),
        "HP: " + character.health + " / " + character.maxHealth,
        "Mana: " + character.mana + " / " + character.maxMana,
        "Init: " + character.init
    ].join('\n');
}
function showStatus(fight) {
    console.log("fight id: " + fight.id);
    if (!fight.finished) {
        console.log('Active fight');
    }
    else {
        console.log("Fight finished and " + (fight.winner === core_1.Team.player ? 'won' : 'lost'));
    }
    var allies = fight.fighters.filter(function (fighter) { return fighter.team === core_1.Team.player; });
    var enemies = fight.fighters.filter(function (fighter) { return fighter.team === core_1.Team.enemy; });
    var activeFighter = fight.getActiveFighter();
    console.log();
    console.log("Allies:\n" + allies.map(function (ally) { return format(ally); }));
    console.log('\n -------------------------------------------- \n');
    console.log("Enemies:\n" + enemies.map(function (enemy) { return format(enemy); }));
    console.log();
    console.log("Active Fighter: " + activeFighter.name + " (" + activeFighter.id + ")");
    if (activeFighter.team === core_1.Team.player) {
        console.log('Your turn');
    }
    else {
        console.log('Enemy turn');
    }
    console.log();
    return fight;
}
exports.showStatus = showStatus;
