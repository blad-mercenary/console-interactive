import { Character, Fight, Selection, Team } from '@blad-mercenary/core';
export declare function getTeam(team: Team): Promise<Character[]>;
export declare function start(savefile?: string, forceNew?: boolean, presetIndex?: number): Promise<void>;
export declare function getPlayerAction(fight: Fight, activeFighter: Character): Promise<Selection>;
export declare function loop(savefile?: string): Promise<boolean>;
export declare function log(savefile?: string): Promise<void>;
