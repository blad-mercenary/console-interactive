"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.noAction = exports.resurrect = exports.heal = exports.damage = exports.playerLoad = exports.playerSave = exports.victory = exports.characterDied = exports.actionUsed = void 0;
var core_1 = require("@blad-mercenary/core");
var print = __importStar(require("./print"));
function name(id, fighters) {
    var fighter = fighters.find(function (fighter) { return fighter.id === id; });
    if (!fighter) {
        throw new Error("Cannot find id " + id);
    }
    return print.name(fighter);
}
function actionUsed(event, fighters) {
    var details = event.details;
    var from = name(details.from, fighters);
    var action = core_1.Action.load(details.action);
    var targets = details.targetsIds.map(function (target) { return name(target, fighters); });
    console.log(from + " used " + action.name + " on " + targets.join(', '));
}
exports.actionUsed = actionUsed;
function characterDied(event) {
    var details = event.details;
    var character = details.characterId;
    console.log(character + " died");
}
exports.characterDied = characterDied;
function victory(event) {
    var details = event.details;
    if (details.winner === core_1.Team.player) {
        console.log('You won');
    }
    else {
        console.log('You lose');
    }
}
exports.victory = victory;
function playerSave() {
    console.log('Player save');
}
exports.playerSave = playerSave;
function playerLoad() {
    console.log('Player load');
}
exports.playerLoad = playerLoad;
function damage(event, fighters) {
    var details = event.details;
    var from = name(details.from, fighters);
    var target = name(details.target, fighters);
    console.log(from + " inflicted " + details.damage + " damage to " + target);
}
exports.damage = damage;
function heal(event, fighters) {
    var details = event.details;
    var from = name(details.from, fighters);
    var target = name(details.target, fighters);
    console.log(from + " healed " + target + " of " + details.heal + " damage");
}
exports.heal = heal;
function resurrect(event, fighters) {
    var details = event.details;
    var from = name(details.from, fighters);
    var target = name(details.target, fighters);
    console.log(from + " resurected " + target);
}
exports.resurrect = resurrect;
function noAction(event, fighters) {
    var details = event.details;
    var from = name(details.from, fighters);
    console.log(from + " did nothing");
}
exports.noAction = noAction;
