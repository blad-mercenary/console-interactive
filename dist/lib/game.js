"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __spreadArray = (this && this.__spreadArray) || function (to, from) {
    for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
        to[j] = from[i];
    return to;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.log = exports.loop = exports.getPlayerAction = exports.start = exports.getTeam = void 0;
var inquirer_1 = __importDefault(require("inquirer"));
var core_1 = require("@blad-mercenary/core");
var savefile_1 = require("./savefile");
var print = __importStar(require("./print"));
var presets_1 = require("./presets");
function getTeam(team) {
    return __awaiter(this, void 0, void 0, function () {
        var characterTypes, characters, stop, characterType;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    characterTypes = Object.keys(core_1.Characters);
                    characters = [];
                    stop = false;
                    _a.label = 1;
                case 1:
                    if (!!stop) return [3, 3];
                    return [4, inquirer_1.default.prompt({
                            type: 'list',
                            name: 'characterType',
                            message: "Add to " + team + "'s team",
                            choices: __spreadArray(__spreadArray([], characterTypes), [{ name: '-Done-', value: null }])
                        })];
                case 2:
                    characterType = (_a.sent()).characterType;
                    if (characterType) {
                        characters.push(core_1.Characters[characterType](team));
                    }
                    else {
                        stop = true;
                    }
                    return [3, 1];
                case 3: return [2, characters];
            }
        });
    });
}
exports.getTeam = getTeam;
function start(savefile, forceNew, presetIndex) {
    return __awaiter(this, void 0, void 0, function () {
        var allies, enemies, fight;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!(forceNew || !savefile)) return [3, 6];
                    allies = void 0;
                    enemies = void 0;
                    if (!(presetIndex !== undefined)) return [3, 1];
                    allies = presets_1.preset[presetIndex].allies;
                    enemies = presets_1.preset[presetIndex].enemies;
                    return [3, 4];
                case 1: return [4, getTeam(core_1.Team.player)];
                case 2:
                    allies = _a.sent();
                    return [4, getTeam(core_1.Team.enemy)];
                case 3:
                    enemies = _a.sent();
                    _a.label = 4;
                case 4:
                    fight = new core_1.Fight(__spreadArray(__spreadArray([], allies), enemies));
                    return [4, savefile_1.save(fight, savefile)];
                case 5:
                    _a.sent();
                    _a.label = 6;
                case 6: return [2];
            }
        });
    });
}
exports.start = start;
function getPlayerAction(fight, activeFighter) {
    return __awaiter(this, void 0, void 0, function () {
        var possibleSelections, action;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    possibleSelections = core_1.Selection.getPossibleSelections(activeFighter, fight);
                    return [4, inquirer_1.default.prompt({
                            type: 'list',
                            name: 'action',
                            message: activeFighter.name + ":",
                            choices: possibleSelections.map(function (selection) {
                                return {
                                    name: print.selection(selection),
                                    value: selection
                                };
                            })
                        })];
                case 1:
                    action = (_a.sent()).action;
                    return [2, action];
            }
        });
    });
}
exports.getPlayerAction = getPlayerAction;
function loop(savefile) {
    return __awaiter(this, void 0, void 0, function () {
        var fight, beforeTurn, activeFighter, action, turnEvents, next;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, savefile_1.load(savefile)];
                case 1:
                    fight = _a.sent();
                    print.status(fight);
                    beforeTurn = new Date();
                    activeFighter = fight.getActiveFighter();
                    if (!(activeFighter.team === core_1.Team.enemy)) return [3, 2];
                    fight.step();
                    return [3, 4];
                case 2: return [4, getPlayerAction(fight, activeFighter)];
                case 3:
                    action = _a.sent();
                    fight.step(action);
                    _a.label = 4;
                case 4:
                    turnEvents = fight.eventLogger.getEvents(beforeTurn);
                    turnEvents.forEach(function (event) {
                        print.event(event, fight.fighters);
                    });
                    console.log('-'.repeat(process.stdout.columns));
                    return [4, savefile_1.save(fight, savefile)];
                case 5:
                    _a.sent();
                    return [4, inquirer_1.default.prompt({ type: 'confirm', name: 'next', message: 'Go to next turn ?' })];
                case 6:
                    next = (_a.sent()).next;
                    return [2, fight.finished || !next];
            }
        });
    });
}
exports.loop = loop;
function log(savefile) {
    return __awaiter(this, void 0, void 0, function () {
        var fight;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, savefile_1.load(savefile)];
                case 1:
                    fight = _a.sent();
                    fight.eventLogger.getEvents().forEach(function (event) {
                        print.event(event, fight.fighters);
                    });
                    return [2];
            }
        });
    });
}
exports.log = log;
