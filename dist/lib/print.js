"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.status = exports.characterBar = exports.event = exports.selection = exports.name = void 0;
var chalk_1 = __importDefault(require("chalk"));
var core_1 = require("@blad-mercenary/core");
var EventLogger = __importStar(require("./event-logger"));
var length = 20;
function teamColor(team) {
    if (team === core_1.Team.player) {
        return chalk_1.default.green;
    }
    else {
        return chalk_1.default.red;
    }
}
function name(character) {
    return teamColor(character.team)(character.name);
}
exports.name = name;
function selection(selection) {
    if (selection.action.possibleTarget === core_1.Target.self) {
        return selection.action.name;
    }
    return selection.action.name + " " + selection.targets.map(function (character) { return character.name; }).join(',');
}
exports.selection = selection;
function event(event, fighters) {
    if (event.type === 'actionUsed') {
        EventLogger.actionUsed(event, fighters);
    }
    if (event.type === 'characterDied') {
        EventLogger.characterDied(event);
    }
    if (event.type === 'victory') {
        EventLogger.victory(event);
    }
    if (event.type === 'playerSave') {
        EventLogger.playerSave();
    }
    if (event.type === 'playerLoad') {
        EventLogger.playerLoad();
    }
    if (event.type === 'damage') {
        EventLogger.damage(event, fighters);
    }
    if (event.type === 'heal') {
        EventLogger.heal(event, fighters);
    }
    if (event.type === 'resurrect') {
        EventLogger.resurrect(event, fighters);
    }
    if (event.type === 'noAction') {
        EventLogger.noAction(event, fighters);
    }
}
exports.event = event;
function formatCharacter(fighter, index) {
    function getBar(cur, max) {
        var percent = cur / max * (length - 2);
        return "[" + '='.repeat(percent) + ' '.repeat(length - 2 - percent) + "]";
    }
    return {
        index: "[" + index + "]",
        id: fighter.id,
        team: fighter.team,
        name: fighter.name,
        type: fighter.type,
        health: fighter.health + " / " + fighter.maxHealth,
        healthBar: getBar(fighter.health, fighter.maxHealth),
        mana: fighter.mana + " / " + fighter.maxMana,
        manaBar: getBar(fighter.mana, fighter.maxMana),
        init: fighter.init + " / " + fighter.lastTurn
    };
}
function printProperty(characters, property, color) {
    if (color === void 0) { color = chalk_1.default.reset; }
    console.log(color(characters.map(function (character) {
        var propertyString = "" + character[property];
        var data = propertyString.length > length
            ? propertyString.slice(0, length)
            : propertyString.padEnd(length);
        return data;
    }).join('')));
}
function characterBar(characters, team) {
    printProperty(characters, 'index', teamColor(team));
    printProperty(characters, 'name', teamColor(team));
    printProperty(characters, 'type');
    printProperty(characters, 'health', chalk_1.default.green);
    printProperty(characters, 'healthBar', chalk_1.default.green);
    printProperty(characters, 'mana', chalk_1.default.blueBright);
    printProperty(characters, 'manaBar', chalk_1.default.blueBright);
    printProperty(characters, 'init', chalk_1.default.yellow);
}
exports.characterBar = characterBar;
function status(fight) {
    var fighters = fight.fighters.map(formatCharacter);
    var allies = fighters.filter(function (fighter) { return fighter.team === core_1.Team.player; });
    var enemies = fighters.filter(function (fighter) { return fighter.team === core_1.Team.enemy; });
    var activeFighters = fight
        .getNextActiveFighters()
        .map((function (aF) { return fighters.find(function (f) { return f.id === aF.id; }); }))
        .map(function (fighter) { return teamColor(fighter.team)(fighter.index); })
        .join('');
    console.log('Allies:');
    characterBar(allies, core_1.Team.player);
    console.log('Enemies:');
    characterBar(enemies, core_1.Team.enemy);
    console.log(activeFighters);
}
exports.status = status;
