"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var yargs_1 = __importDefault(require("yargs"));
var game_1 = require("../lib/game");
var presets_1 = require("../lib/presets");
var args = yargs_1.default
    .option('new', {
    alias: 'n',
    type: 'boolean',
    description: 'Create a new fight, deleting the current savefile.'
})
    .option('preset', {
    alias: 'p',
    type: 'number',
    description: "Use fight preset (from 0 to " + (presets_1.preset.length - 1) + "). Implies --new."
})
    .option('file', {
    alias: 'f',
    type: 'string',
    description: 'Path of the savefile to use.'
})
    .option('logs', {
    type: 'boolean',
    description: 'Pretty print the fight\'s logs.'
})
    .argv;
if (args.preset !== undefined) {
    if (args.preset < 0 || args.preset >= presets_1.preset.length) {
        throw new Error('Invalid preset');
    }
    args.new = true;
}
function run(args) {
    return __awaiter(this, void 0, void 0, function () {
        var end;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, game_1.start(args.file, args.new, args.preset)];
                case 1:
                    _a.sent();
                    end = false;
                    _a.label = 2;
                case 2:
                    if (!!end) return [3, 4];
                    return [4, game_1.loop()];
                case 3:
                    end = _a.sent();
                    return [3, 2];
                case 4: return [2];
            }
        });
    });
}
if (args.logs) {
    game_1.log(args.file);
}
else {
    run(args).catch(function (error) {
        console.error(error);
        process.exit(-1);
    });
}
