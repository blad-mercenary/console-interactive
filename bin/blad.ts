import yargs from 'yargs';
import { start, loop, log } from '../lib/game';
import { preset } from '../lib/presets';

interface Args {
	new?: boolean,
	preset?: number,
	file?: string,
	logs?: boolean
}

const args = yargs
	.option('new', {
		alias: 'n',
		type: 'boolean',
		description: 'Create a new fight, deleting the current savefile.'
	})
	.option('preset', {
		alias: 'p',
		type: 'number',
		description: `Use fight preset (from 0 to ${preset.length - 1}). Implies --new.`
	})
	.option('file', {
		alias: 'f',
		type: 'string',
		description: 'Path of the savefile to use.'
	})
	.option('logs', {
		type: 'boolean',
		description: 'Pretty print the fight\'s logs.'
	})
	.argv;

if (args.preset !== undefined) {
	if (args.preset < 0 || args.preset >= preset.length) {
		throw new Error('Invalid preset');
	}
	args.new = true;
}

async function run(args: Args) {
	await start(args.file, args.new, args.preset);
	let end = false;
	while (!end) {
		end = await loop();
	}
}

if (args.logs) {
	log(args.file);
} else {
	run(args).catch((error) => {
		console.error(error);
		process.exit(-1);
	});
}
